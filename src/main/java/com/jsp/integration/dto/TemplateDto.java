package com.jsp.integration.dto;

import java.sql.Date;

public class TemplateDto {

	private long altKey;
	private String templateCode;
	private String entityId;
	private String templateSubject;
	private String templateContent;
	private String templatePlaceholder;
	private Date createdDate;
	private String createdBy;
	private Date ModifiedDate;
	private String ModifiedBy;
	public long getAltKey() {
		return altKey;
	}
	public void setAltKey(long altKey) {
		this.altKey = altKey;
	}
	public String getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getTemplateSubject() {
		return templateSubject;
	}
	public void setTemplateSubject(String templateSubject) {
		this.templateSubject = templateSubject;
	}
	public String getTemplateContent() {
		return templateContent;
	}
	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}
	public String getTemplatePlaceholder() {
		return templatePlaceholder;
	}
	public void setTemplatePlaceholder(String templatePlaceholder) {
		this.templatePlaceholder = templatePlaceholder;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return ModifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		ModifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return ModifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}
	@Override
	public String toString() {
		return "TemplateDto [altKey=" + altKey + ", templateCode=" + templateCode + ", entityId=" + entityId
				+ ", templateSubject=" + templateSubject + ", templateContent=" + templateContent
				+ ", templatePlaceholder=" + templatePlaceholder + ", createdDate=" + createdDate + ", createdBy="
				+ createdBy + ", ModifiedDate=" + ModifiedDate + ", ModifiedBy=" + ModifiedBy + "]";
	}
	
}

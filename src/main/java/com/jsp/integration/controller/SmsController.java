package com.jsp.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jsp.integration.dto.CustomerDetailsDto;
import com.jsp.integration.dto.TemplateDto;
import com.jsp.integration.service.SmsServiceImpl;

@RestController
public class SmsController {
	
	@Autowired
	private SmsServiceImpl smsServiceImpl;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@PostMapping(value="/createTemplate")
	public @ResponseBody String createTemplate(@RequestBody TemplateDto templateDto) {
		String string = smsServiceImpl.processCreateTemplate(templateDto,jdbcTemplate );
		return string;
	}
	
	@PostMapping(value="/createCustomer")
	public String createCustomerDetails(@RequestBody CustomerDetailsDto customerDetailsDto) {
		String string = smsServiceImpl.processCreateCustomerDetails(customerDetailsDto,jdbcTemplate );
		return string;
	}

	@PostMapping(value="/sendSms")
	public @ResponseBody String sendSms(@RequestParam String templateCode,@RequestParam long uniqueId) {
		String string = smsServiceImpl.processSendSms(templateCode,uniqueId,jdbcTemplate);
		return string;
	}
}

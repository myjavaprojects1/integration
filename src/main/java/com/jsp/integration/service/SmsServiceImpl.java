package com.jsp.integration.service;


import java.io.IOException;
import java.sql.Date;
import java.util.ArrayDeque;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsp.integration.dto.CustomerDetailsDto;
import com.jsp.integration.dto.TemplateDto;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;


@Service
public class SmsServiceImpl implements SmsService{

	private final static String ACCOUNT_SID="AC338faf5a3ade8b20e818dbf2d96271a5";
	private final static String AUTH_ID="8e03a285c1775bfc519ac9e1a77029ba";

	static {
		   Twilio.init(ACCOUNT_SID, AUTH_ID);
		}
	private TemplateDto templateProperties;
	@Autowired
	private ObjectMapper objectMapper;
	
	public String processCreateTemplate(TemplateDto templateDto,JdbcTemplate jdbcTemplate ) {
		StringBuilder build = new StringBuilder();
		String string = templateDto.getTemplateContent();
		Encoder encoder = Base64.getEncoder();
		String encodedString = encoder.encodeToString(string.getBytes());
		
		build.append("Insert into template_details (alt_key,template_code,entity_id,template_subject,template_content,template_placeholder,created_date,created_by,modified_date,modified_by) values"
				+ "("+templateDto.getAltKey()+",'"+templateDto.getTemplateCode()+"','"+templateDto.getEntityId()+"','"+templateDto.getTemplateSubject()+"','"+encodedString+"','"+templateDto.getTemplatePlaceholder()+"','"+templateDto.getCreatedDate()+"','"+templateDto.getCreatedBy()+"',"+null+","+null+")");
		jdbcTemplate.execute(build.toString());
		
		return new String("the template stored succesfully");
	}
	
	public String processCreateCustomerDetails(CustomerDetailsDto customerDetailsDto,JdbcTemplate jdbcTemplate ) {
		StringBuilder build = new StringBuilder();
		build.append("Insert into customer_details (unique_id,name,tele_number,sms_status) values('"+customerDetailsDto.getUniqueId()+"','"+customerDetailsDto.getName()+"','"+customerDetailsDto.getTeleNumber()+"','"+customerDetailsDto.getSmsStatus()+"')");
		jdbcTemplate.update(build.toString());
		return new String("the customerDetails stored succesfully");
	}
	
	
	
	@Override
	public String processSendSms(String templateCode,long uniqueId,JdbcTemplate jdbcTemplate) {
		StringBuilder build = new StringBuilder();
		build.append("select * from template_details where template_code='"+templateCode+"'");
		Map<String, Object> each = jdbcTemplate.queryForMap(build.toString());
		
					TemplateDto templateDto = new TemplateDto();
					templateDto.setAltKey(Long.parseLong(each.get("alt_key").toString()));
					templateDto.setEntityId(each.get("entity_id").toString());
					templateDto.setTemplateCode(each.get("template_code").toString());
					templateDto.setTemplatePlaceholder(each.get("template_placeholder").toString());
					templateDto.setTemplateContent(each.get("template_content").toString());
					templateDto.setTemplateSubject(each.get("template_subject").toString());
					templateDto.setCreatedDate((Date)each.get("created_date"));
					templateDto.setCreatedBy(each.get("created_by").toString());

		StringBuilder builder = new StringBuilder();
		builder.append("select * from customer_details where unique_id="+uniqueId+"");
	    Map<String, Object> each1 = jdbcTemplate.queryForMap(builder.toString());
					CustomerDetailsDto customerDetailsDto = new CustomerDetailsDto();
					customerDetailsDto.setName(each1.get("name").toString());
					customerDetailsDto.setTeleNumber(each1.get("tele_number").toString());
					customerDetailsDto.setUniqueId(Long.parseLong(each1.get("unique_id").toString()));
//					customerDetailsDto.setSmsStatus(each.get("sms_status").toString());
					
					Map<String, Object> customerDetails = objectMapper.convertValue(customerDetailsDto,new TypeReference<Map<String,Object>>() {});
					Set<String> keySet = customerDetails.keySet();		

		try {
			List<Map<String, String>> readValue = objectMapper.readValue( templateDto.getTemplatePlaceholder(), List.class);
			int count=0;
			for (Map<String,String> map :readValue) {
				count++;
					for(String key:keySet) {
						if(key.equals("name")&& count==1){
							map.put("entityfieldname",key);
							map.put("placeholdkey",(String)customerDetails.get(key));
						}
						
						if(key.equals("teleNumber")&& count==2){
							map.put("entityfieldname",key);
							map.put("placeholdkey",(String)customerDetails.get(key));
						}
					}
					
			}
			String content = new String( Base64.getDecoder().decode(templateDto.getTemplateContent()));
			String s=content.replace("${name}", readValue.get(0).get("placeholdkey")).replace("${number}", readValue.get(1).get("placeholdkey"));
			System.out.println(s);
			
//			Message.creator(new PhoneNumber(customerDetailsDto.getTeleNumber()), new PhoneNumber("+1 236 304 6793"),
//					   s).create();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		
		return new String("the message sented succesfully");
	
	}
}

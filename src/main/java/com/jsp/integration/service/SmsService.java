package com.jsp.integration.service;

import org.springframework.jdbc.core.JdbcTemplate;

import com.jsp.integration.dto.CustomerDetailsDto;
import com.jsp.integration.dto.TemplateDto;

public interface SmsService {

	public String processCreateTemplate(TemplateDto templateDto,JdbcTemplate jdbcTemplate );
	public String processCreateCustomerDetails(CustomerDetailsDto customerDetailsDto,JdbcTemplate jdbcTemplate );
	public String processSendSms(String templateCode,long uniqueId ,JdbcTemplate jdbcTemplate);
}
